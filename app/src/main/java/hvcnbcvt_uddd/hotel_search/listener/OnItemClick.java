package hvcnbcvt_uddd.hotel_search.listener;

import android.view.View;

public interface OnItemClick {
    void onItemClick(View view, int pos);
}
