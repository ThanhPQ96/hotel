package hvcnbcvt_uddd.hotel_search.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.ReviewAdapter_search;
import hvcnbcvt_uddd.hotel_search.model.Review_search;

public class ReviewFragment_search extends Fragment {
    private ArrayList<Review_search> arrReview;
    private ReviewAdapter_search reviewAdapterSearch;
    private RecyclerView rcvReview;
    public ReviewFragment_search() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_review_,container,false);

        rcvReview = view.findViewById(R.id.rcv_review);

        inItData();
        inItView();
        return view;
    }

    private void inItView(){
        reviewAdapterSearch = new ReviewAdapter_search(arrReview,getActivity());
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvReview.setLayoutManager(layoutManager);
        rcvReview.setAdapter(reviewAdapterSearch);
    }

    private void inItData(){
        arrReview = new ArrayList<>();
        arrReview.add(new Review_search("Phạm Quang Thanh","Phục vụ rất tốt, không gian thoáng đáng.",R.drawable.man_review_search));
        arrReview.add(new Review_search("Nguyền Thị A","Nhà vệ sinh quá bẩn :(((.",R.drawable.woman_review_search));
    }
}