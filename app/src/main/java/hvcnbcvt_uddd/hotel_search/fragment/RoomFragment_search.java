package hvcnbcvt_uddd.hotel_search.fragment;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.RoomAdapter_search;
import hvcnbcvt_uddd.hotel_search.listener.OnItemClick;
import hvcnbcvt_uddd.hotel_search.model.Room_search;
import hvcnbcvt_uddd.hotel_search.view.Buoc1_bill;
import hvcnbcvt_uddd.hotel_search.view.Manager_Room;

public class RoomFragment_search extends Fragment {
    private ArrayList<Room_search> arrRoom;
    private RoomAdapter_search roomAdapterSearch;
    private RecyclerView rcvRoom;
    private DatabaseHelper databaseHelper;
    public RoomFragment_search() {
        // Required empty public constructor
        databaseHelper = new DatabaseHelper(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment_price,container,false);

        rcvRoom = view.findViewById(R.id.rcv_room);
//        rcvRoom.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent i=new Intent(RoomFragment_search.this,Buoc1_bill.class);
//                startActivity(i);
//            }
//        });

        inItData();
        inItView();
        return view;
    }

    private void inItView(){
        roomAdapterSearch = new RoomAdapter_search(arrRoom,getActivity(),onItemClick);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        rcvRoom.setLayoutManager(layoutManager);
        rcvRoom.setAdapter(roomAdapterSearch);
    }

    private void inItData(){
        arrRoom = new ArrayList<>();

        Cursor cursor = Manager_Room.databaseHelper.getRoom();
        arrRoom.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String bed=cursor.getString(3);
            String number= cursor.getString(4);
            String price = cursor.getString(5);
            byte[] image = cursor.getBlob(2);
            String id_hotel=cursor.getString(6);

            arrRoom.add(new Room_search(String.valueOf(id),name,bed,number,id_hotel,Float.parseFloat(price)));
        }
    }

    private OnItemClick onItemClick = new OnItemClick() {
        @Override
        public void onItemClick(View view, int pos) {
            Intent i=new Intent(getActivity(),Buoc1_bill.class);
            startActivity(i);
        }
    };
}
