package hvcnbcvt_uddd.hotel_search.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Account;

/**
 * Created by hoacn on 5/19/2018.
 */

public class CreateAccount extends AppCompatActivity implements View.OnClickListener{
    private EditText editText1, editText2, editText3, editText4, editText5, editText6;
    private Button btn1, btn2;
    DatabaseHelper helper = new DatabaseHelper(this);
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_dialog_create_acc);


        btn1 = (Button) findViewById(R.id.btn_dangky);
        btn2 = (Button) findViewById(R.id.btn_dangnhap);

        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_dangky:


                    editText1 = (EditText) findViewById(R.id.ed_email);
                    editText2 = (EditText) findViewById(R.id.ed_pass);
                    editText6 = (EditText) findViewById(R.id.ed_repass);
                    editText3 = (EditText) findViewById(R.id.ed_name);
                    editText4 = (EditText) findViewById(R.id.ed_sdt);
                    editText5 = (EditText) findViewById(R.id.ed_dchi);

                    String user = editText1.getText().toString();
                    String pass = editText2.getText().toString();
                    String name = editText3.getText().toString();
                    String sdt = editText4.getText().toString();
                    String dchi = editText5.getText().toString();
                    String repass = editText6.getText().toString();
                    if (!pass.equals(repass)){
                        Toast.makeText(this, "Nhập sai mật khẩu", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        Account acc = new Account();
                        acc.setUsername(user);
                        acc.setUser_pass(pass);
                        acc.setRepass(repass);
                        acc.setFullname(name);
                        acc.setSdt(Integer.parseInt(sdt));
                        acc.setDiachi(dchi);
                        Account temp = helper.Checkuser(user);
                        if (temp == null){
                            helper.insertAccount(acc);
                            Toast.makeText(CreateAccount.this,"Đăng ký thành công", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
                            builder.setTitle("Error");
                            builder.setMessage("Da ton tai user");
                            builder.setPositiveButton("oki", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            });
                            builder.show();
                        }
                    }



                break;
            case R.id.btn_dangnhap:
                Intent intent = new Intent(CreateAccount.this, Login.class);
                startActivity(intent);
                break;
        }
    }
}
