package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import hvcnbcvt_uddd.hotel_search.R;

/**
 * Created by NgocTri on 5/19/2018.
 */

public class Buoc1_bill extends AppCompatActivity implements View.OnClickListener{
    Button btn_Show_on_map,btn_Next_step;
    ImageButton btn_Undo1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_buoc1);
        initView();
        btn_Undo1.setOnClickListener(this);
        btn_Show_on_map.setOnClickListener(this);
        btn_Next_step.setOnClickListener(this);
    }

    public void initView(){
        btn_Next_step=(Button)findViewById(R.id.btn_Next_step_of_1);
        btn_Show_on_map=(Button)findViewById(R.id.btn_show_on_map);
        btn_Undo1=(ImageButton)findViewById(R.id.btn_Undo_b1);
    }
    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btn_Next_step_of_1:{
                Intent i1=new Intent(Buoc1_bill.this,Buoc2_bill.class);
                startActivity(i1);
                break;
            }
            case R.id.btn_show_on_map:{
                Intent i2= new Intent(Buoc1_bill.this,Show_on_map_bill.class);
                startActivity(i2);
                break;
            }
            case R.id.btn_Undo_b1:{
                Intent i3= new Intent(Buoc1_bill.this,MainActivity_home.class);
                startActivity(i3);
                break;
            }
        }
    }
}
