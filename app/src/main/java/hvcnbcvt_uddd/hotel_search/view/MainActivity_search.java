package hvcnbcvt_uddd.hotel_search.view;

import android.app.DatePickerDialog;
import android.database.Cursor;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.SearchView;
import android.widget.Spinner;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.HotelAdapter_search;
import hvcnbcvt_uddd.hotel_search.model.Hotel;
import hvcnbcvt_uddd.hotel_search.model.Hotel_search;

public class MainActivity_search extends AppCompatActivity implements  SearchView.OnQueryTextListener {

    Button btnCheckin, btnCheckout;
    Spinner spinnerRoom;
    Calendar calendar = Calendar.getInstance();
    List<String> list = new ArrayList<>();
    ArrayAdapter<String> adapter;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    SearchView searchViewhotel;
    private RecyclerView recyclerView;
    private HotelAdapter_search hotelAdapterSearch;
    private ArrayList<Hotel_search> arrHotelSearch;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity_main);
        searchViewhotel=findViewById(R.id.searchhotel);
        searchViewhotel.setOnQueryTextListener(this);
        addControl();
        addEvent();

        inItData();
        inItView();
    }

    private void inItData(){
        recyclerView = (RecyclerView) findViewById(R.id.rcv_hotel);
        arrHotelSearch = new ArrayList<>();

        // get all data from sqlite
        Cursor cursor = MainActivity_home.sqLiteHelper.getData("SELECT * FROM Hotel");
        arrHotelSearch.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String adress = cursor.getString(2);
            byte[] image = cursor.getBlob(3);
            String number = cursor.getString(4);
            String vote = cursor.getString(5);
            arrHotelSearch.add(new Hotel_search(String.valueOf(id),name, adress, image, Integer.parseInt(number), vote,"0869221702"));
        }
    }

    private void inItView(){
        hotelAdapterSearch = new HotelAdapter_search(arrHotelSearch,this);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(hotelAdapterSearch);
    }

    private void addEvent() {
        btnCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyHienThiDatePicker1();
            }
        });
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                xuLyHienThiDatePicker2();
            }
        });
        list.add("Single Room");
        list.add("Double Room");
        list.add("Family");
        adapter = new ArrayAdapter<>(this,R.layout.support_simple_spinner_dropdown_item,list);
        spinnerRoom.setAdapter(adapter);
        spinnerRoom.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    //Lay ngay checkin
    private void xuLyHienThiDatePicker1() {
        DatePickerDialog.OnDateSetListener callBack = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(Calendar.YEAR,i);
                calendar.set(Calendar.MONTH,i1);
                calendar.set(Calendar.DATE,i2);
                btnCheckin.setText(simpleDateFormat.format(calendar.getTime()));

            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity_search.this,callBack,
                calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE));
        datePickerDialog.show();
    }
    //Lay ngay checkout
    private void xuLyHienThiDatePicker2() {
        DatePickerDialog.OnDateSetListener callBack = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                calendar.set(Calendar.YEAR,i);
                calendar.set(Calendar.MONTH,i1);
                calendar.set(Calendar.DATE,i2);
                btnCheckout.setText(simpleDateFormat.format(calendar.getTime()));

            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(MainActivity_search.this,callBack,
                calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DATE));
        datePickerDialog.show();
    }

    private void addControl() {
        btnCheckin = (Button) findViewById(R.id.btnCheckin);
        btnCheckout = (Button) findViewById(R.id.btnCheckout);
        spinnerRoom = (Spinner) findViewById(R.id.spinnerRoom);
        calendar = Calendar.getInstance();
    }
    //Tìm kiếm khách sạn
    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        newText = newText.toLowerCase();
        ArrayList<Hotel_search> dsks = new ArrayList<>();
        for (Hotel_search hotelSearch : arrHotelSearch) {
            String name = hotelSearch.getName_hotel().toLowerCase();
            String address = hotelSearch.getAddress_hotel().toLowerCase();
            if ((address.contains(newText)) || (name.contains(newText))) {
                dsks.add(hotelSearch);
            }
        }
        hotelAdapterSearch.setFilter(dsks);
        return true;
    }

}

