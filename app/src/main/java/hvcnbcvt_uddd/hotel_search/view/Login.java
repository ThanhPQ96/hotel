package hvcnbcvt_uddd.hotel_search.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Account;

public class Login extends AppCompatActivity implements View.OnClickListener{
    private EditText editText1, editText2;
    private Button btnlogin;
    DatabaseHelper helper ;
    private TextView txt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_dialog_signin);
        editText1 = (EditText) findViewById(R.id.ed_email1);
        editText2 = (EditText) findViewById(R.id.ed_pass1);
        btnlogin = (Button) findViewById(R.id.login);
        btnlogin.setOnClickListener(this);
        txt = (TextView) findViewById(R.id.dki);
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Login.this,CreateAccount.class);
                startActivity(intent);
                finish();
            }
        });
    }

    @Override
    public void onClick(View v) {
        DatabaseHelper helper = new DatabaseHelper(getApplicationContext());
      String user = editText1.getText().toString();
      String pass = editText2.getText().toString();
      Account account = helper.Login(user, pass);

      if (account==null){

          AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
          builder.setTitle("Error");
          builder.setMessage("Sai tên đăng nhập hoặc maatj khẩu");
          builder.setPositiveButton("oki", new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                  dialog.cancel();
              }
          });
          builder.show();

      }
      else {

          Intent itend = new Intent(Login.this, MainActivity_home.class);
          itend.putExtra("User", user);

          startActivity(itend);
      }

    }
}