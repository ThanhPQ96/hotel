package hvcnbcvt_uddd.hotel_search.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.NullTimeListAdapter;
import hvcnbcvt_uddd.hotel_search.model.NullTime;

/**
 *
 */

public class NullTimeList extends AppCompatActivity {

    GridView gridView;
    ArrayList<NullTime> list;
    NullTimeListAdapter adapter = null;



    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nulltime_list_activity);

        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<>();
        adapter = new NullTimeListAdapter(this, R.layout.nulltime_items, list);
        gridView.setAdapter(adapter);

        // get all data from sqlite
        Cursor cursor = NullTime_MainActivity.sqLiteHelper.getData("SELECT * FROM NullTime");
        list.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            int id_room = cursor.getInt(1);
            String startDate = cursor.getString(2);
            String endDate = cursor.getString(3);

            list.add(new NullTime(id, id_room, startDate, endDate));
        }
        adapter.notifyDataSetChanged();

        gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id) {
                CharSequence[] items = {"Update", "Delete"};
                AlertDialog.Builder dialog = new AlertDialog.Builder(NullTimeList.this);

                dialog.setTitle("Choose an action");
                dialog.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int item) {
                        if (item == 0) {
                            // update
                            Cursor c = NullTime_MainActivity.sqLiteHelper.getData("SELECT id_nulltime FROM NullTime");
                            ArrayList<Integer> arrID = new ArrayList<Integer>();
                            while (c.moveToNext()){
                                arrID.add(c.getInt(0));
                            }
                            // show dialog update at here
                            //  showDialogUpdate(HotelList.this, arrID.get(position));

                        } else {
                            // delete
                            Cursor c = NullTime_MainActivity.sqLiteHelper.getData("SELECT id_nulltime FROM NullTime");
                            ArrayList<Integer> arrID = new ArrayList<Integer>();
                            while (c.moveToNext()){
                                arrID.add(c.getInt(0));
                            }
                         //   showDialogDelete(arrID.get(position));
                        }
                    }
                });
                dialog.show();
                return true;
            }
        });

    }







    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {



        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
    }
}