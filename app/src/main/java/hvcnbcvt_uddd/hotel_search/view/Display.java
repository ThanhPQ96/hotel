package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import hvcnbcvt_uddd.hotel_search.R;

public class Display extends AppCompatActivity {
    private TextView textView;
    private Button button;
    public static final String USER = "USER";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_display);
//        setContentView(R.layout.activity_main_home);
        textView = (TextView) findViewById(R.id.display1);
      // Intent intent = getIntent();

      // final Account account = (Account) intent.getSerializableExtra("User");
      final String username = getIntent().getStringExtra("User");

        textView.setText(username);
        button = (Button) findViewById(R.id.btnUpdate);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             /* Intent i = new Intent(Display.this, UpadateAccount.class);
                i.putExtra("account", (Serializable) account);
                startActivity(i);*/

             Intent intent = new Intent(Display.this,MainActivity_home.class);
             intent.putExtra("User",username);
             startActivity(intent);
             finish();
            }
        });

    }


}
