package hvcnbcvt_uddd.hotel_search.view;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;

/**
 * Created by NgocTri on 5/22/2018.
 */

public class Manager_Room extends AppCompatActivity implements View.OnClickListener{

    EditText edtName,edtPrice,edtBed,edtNumber,edtIDHotel;
    Button btnChoose,btnAdd,btnListRoom;
    ImageView imageRoom;
    final  int REQUEST_CODE=111;
    public static DatabaseHelper databaseHelper;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_room);
        initView();
        btnAdd.setOnClickListener(this);
        btnChoose.setOnClickListener(this);
        btnListRoom.setOnClickListener(this);
        databaseHelper = new DatabaseHelper(this,"Hotel.db",null,1);
        databaseHelper.queryData("CREATE TABLE IF NOT EXISTS ROOM1 (id_room INTEGER PRIMARY KEY AUTOINCREMENT, name_room VARCHAR, image_room BLOB, bed VARCHAR, number_people VARCHAR, price_room VARCHAR, id_hotel VARCHAR)");
        databaseHelper.queryData("CREATE TABLE IF NOT EXISTS Hotel3(id_hotel INTEGER PRIMARY KEY AUTOINCREMENT, name_hotel VARCHAR, address_hotel VARCHAR, image BLOB, number_room VARCHAR , vote VARCHAR, phoneNumber VARCHAR)");

    }

    public static byte[] imageViewToByte(ImageView img){
        Bitmap bitmap= ((BitmapDrawable)img.getDrawable()).getBitmap();
        ByteArrayOutputStream stream= new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,stream);
        byte[] byteArray = stream.toByteArray();
        return byteArray;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if(requestCode == REQUEST_CODE){
            if(grantResults.length >0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent, REQUEST_CODE);
            }
            else {
                Toast.makeText(getApplicationContext(), "You don't have permission to access file location!", Toast.LENGTH_SHORT).show();
            }
            return;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if(requestCode == REQUEST_CODE & resultCode == RESULT_OK && data != null){
            Uri uri = data.getData();

            try {
                InputStream inputStream = getContentResolver().openInputStream(uri);

                Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
                imageRoom.setImageBitmap(bitmap);

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Choose_img_Room:{
                ActivityCompat.requestPermissions(Manager_Room.this,new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},REQUEST_CODE);
                break;
            }
            case R.id.btn_Add_room:{
                   try {
                       databaseHelper.insertRoom(edtName.getText().toString().trim(),
                               imageViewToByte(imageRoom),
                               edtBed.getText().toString().trim(),
                               edtNumber.getText().toString().trim(),
                               edtPrice.getText().toString().trim(),
                               edtIDHotel.getText().toString().trim());
                       Toast.makeText(getApplicationContext(), "Add room thành công !", Toast.LENGTH_LONG).show();

                       edtName.setText("");
                       edtPrice.setText("");
                       edtNumber.setText("");
                       edtIDHotel.setText("");
                       edtBed.setText("");
                       imageRoom.setImageResource(R.mipmap.ic_launcher);
                   }catch (Exception e){
                        e.printStackTrace();
                        Toast.makeText(getApplicationContext(), "Add room fail !", Toast.LENGTH_SHORT).show();
                    }
                    break;

            }
            case R.id.btn_List_room:{
                Intent intent2= new Intent(this,Manager_RoomList.class);
                startActivity(intent2);
                break;
            }
            default:
                break;
        }

    }

    public void initView(){
        edtBed=(EditText)findViewById(R.id.edt_Room_bed);
        edtName=(EditText)findViewById(R.id.edt_Room_name);
        edtNumber=(EditText)findViewById(R.id.edt_Room_numberPeople);
        edtPrice=(EditText)findViewById(R.id.edt_Room_price);
        edtIDHotel=(EditText)findViewById(R.id.edt_Room_id_hotel);
        btnAdd=(Button)findViewById(R.id.btn_Add_room);
        btnListRoom=(Button)findViewById(R.id.btn_List_room);
        btnChoose=(Button)findViewById(R.id.btn_Choose_img_Room);
        imageRoom=(ImageView)findViewById(R.id.img_Room);
    }
}
