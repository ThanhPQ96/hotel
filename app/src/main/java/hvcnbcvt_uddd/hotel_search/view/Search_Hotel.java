package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.HotelListAdapter;
import hvcnbcvt_uddd.hotel_search.model.Hotel;

/**
 * Created by NgocTri on 5/23/2018.
 */

public class Search_Hotel extends AppCompatActivity {
    GridView gridView;
    ArrayList<Hotel> list;
    HotelListAdapter adapter = null;
    Button button;
    String name;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_hotelist);
        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<>();

        Intent i= getIntent();
        name = i.getStringExtra("name");
        Toast.makeText(this, name, Toast.LENGTH_SHORT).show();

        // get all data from sqlite
        Cursor cursor = MainActivity_home.sqLiteHelper.getData("SELECT * FROM Hotel3 WHERE address_hotel LIKE '%"+name+"%'");
        list.clear();
        while (cursor.moveToNext()) {
            int id = cursor.getInt(0);
            String name = cursor.getString(1);
            String adress = cursor.getString(2);
            byte[] image = cursor.getBlob(3);
            String number = cursor.getString(4);
            String vote = cursor.getString(5);
            String phonnumer=cursor.getString(6);
            list.add(new Hotel(name, adress, image, number, vote,phonnumer));
        }
        adapter = new HotelListAdapter(this, R.layout.manager_hotelitem, list);
        gridView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i1= new Intent(Search_Hotel.this,Search_Room.class);
                int id_hotel =(Integer) list.get(position).getId_hotel();
                i1.putExtra("Id",id_hotel);//truyen sang itent Tim phong voi id cua khasc san
                startActivity(i1);
            }
        });
    }
}
