package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.RecycleAdapter1_home;
import hvcnbcvt_uddd.hotel_search.adapter.RecycleAdapter_home;
import hvcnbcvt_uddd.hotel_search.model.Cart;

public class MainActivity_home extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private ImageButton i1, i2, i3, i4, i5, i6;
    private TextView UserName, GmailUser;
    private ImageView img_nav_account;

    private ViewFlipper mViewFlipper;
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();
    private ArrayList<String> mNames1 = new ArrayList<>();
    private ArrayList<String> mImageUrls1 = new ArrayList<>();
    private ArrayList<String> mNames2 = new ArrayList<>();
    private ArrayList<String> mImageUrl2 = new ArrayList<>();
    public static DatabaseHelper sqLiteHelper;
    public static ArrayList<Cart> manggiohang;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_home);
        sqLiteHelper = new DatabaseHelper(this, "Hotel.db", null, 1);
//        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS Hotel2(id_hotel INTEGER PRIMARY KEY AUTOINCREMENT, name_hotel VARCHAR, address_hotel VARCHAR, image BLOB, number_room VARCHAR , vote VARCHAR, phoneNumber VARCHAR)");
        Integer[] imageId = {R.drawable.banner4, R.drawable.banner3, R.drawable.banner2, R.drawable.banner1};
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer);
        onClickImageButton();

        mActionBarDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, R.string.open, R.string.close);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        mActionBarDrawerToggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        NavigationView navigationView = (NavigationView) findViewById(R.id.navigation_view);
        View header = navigationView.getHeaderView(0);

        UserName = (TextView) header.findViewById(R.id.userName);
        String username = getIntent().getStringExtra("User");
        UserName.setText(username);
        GmailUser = (TextView) header.findViewById(R.id.userGmail);
        GmailUser.setText(username+ "@gmail.com");
        img_nav_account = header.findViewById(R.id.img_nav_account);
        img_nav_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity_home.this, Login.class);
                startActivity(i);
            }
        });

        navigationView.setNavigationItemSelectedListener(this);
        int image[] = {R.drawable.banner1, R.drawable.banner2, R.drawable.banner3, R.drawable.banner4};
        mViewFlipper = (ViewFlipper) findViewById(R.id.flipper1);
        for (int images : image) {
            flipperImage(images);
        }
        initImage();
        initImage1();
        initImage2();
        if (manggiohang != null){

        }
        else {
            manggiohang = new ArrayList<>();
        }

    }


    public void onClickImageButton() {
        i1 = (ImageButton) findViewById(R.id.imageView);
        i2 = (ImageButton) findViewById(R.id.imageView1);
        i3 = (ImageButton) findViewById(R.id.imageView2);
        i4 = (ImageButton) findViewById(R.id.imageView3);
        i5 = (ImageButton) findViewById(R.id.imageView4);
        i6 = (ImageButton) findViewById(R.id.imageView5);


        i1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity_home.this, MainActivity_search.class);
                startActivity(i);
                Toast.makeText(MainActivity_home.this, "You are click", Toast.LENGTH_SHORT).show();
            }
        });
        i2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i2= new Intent(MainActivity_home.this,Manager_Room.class);
                startActivity(i2);
                Toast.makeText(MainActivity_home.this, "You are click", Toast.LENGTH_SHORT).show();
            }
        });
        i3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity_home.this,NullTime_MainActivity.class);
                startActivity(i);
            }
        });
        i4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity_home.this, Manager_Hotel_List.class);
                startActivity(intent);
            }
        });
        i5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i=new Intent(MainActivity_home.this,Manager_Hotel.class);
                startActivity(i);
            }
        });
        i6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MainActivity_home.this, Search_Main.class);
                startActivity(i);
            }
        });

    }

    public void flipperImage(int image) {
        ImageView imageView = new ImageView(this);
        imageView.setBackgroundResource(image);
        mViewFlipper.addView(imageView);
        mViewFlipper.setAutoStart(true);
        mViewFlipper.setInAnimation(this, android.R.anim.slide_in_left);
        mViewFlipper.setOutAnimation(this, android.R.anim.slide_out_right);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mActionBarDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();
        if (id == R.id.languages) {
            Toast.makeText(this, "This is home", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.money) {
            Toast.makeText(this, "This is setting", Toast.LENGTH_SHORT).show();

        }

        return true;

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.book) {
            Toast.makeText(this, "This is home", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.discount) {
            Toast.makeText(this, "This is setting", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.log) {
            Toast.makeText(this, "This is logout", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.comment) {
            Toast.makeText(this, "This is home", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.add) {
            Toast.makeText(this, "This is setting", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.setting) {
            Toast.makeText(this, "This is logout", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.help) {
            Toast.makeText(this, "This is home", Toast.LENGTH_SHORT).show();

        }
        if (id == R.id.share) {
            Toast.makeText(this, "This is setting", Toast.LENGTH_SHORT).show();

        }

        return false;
    }

    private void initImage() {
        mImageUrls.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_04_hd_pictures_166229.jpg");
        mNames.add("Đà Nẵng");
        mImageUrls.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_01_hd_picture_166232.jpg");
        mNames.add("Nha Trang");
        mImageUrls.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_02_hd_picture_166206.jpg");
        mNames.add("Vinh");
        mImageUrls.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_03_hd_picture_166205.jpg");
        mNames.add("Đà Lạt");
        mImageUrls.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_02_hd_picture_166231.jpg");
        mNames.add("Huế");
        initRecyleView();
    }

    private void initRecyleView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(layoutManager);
        RecycleAdapter_home adpter = new RecycleAdapter_home(this, mNames, mImageUrls);
        recyclerView.setAdapter(adpter);
    }

    private void initImage1() {
        mImageUrls1.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_04_hd_pictures_166229.jpg");
        mNames1.add("Da Nang");
        mImageUrls1.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_01_hd_picture_166232.jpg");
        mNames1.add("Da Nang1");
        mImageUrls1.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_02_hd_picture_166206.jpg");
        mNames1.add("Da Nang2");
        mImageUrls1.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_03_hd_picture_166205.jpg");
        mNames1.add("Da Nang2");
        mImageUrls1.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_04_hd_pictures_166229.jpg");
        mNames1.add("Da Nang2");
        initRecyleView1();
    }

    private void initRecyleView1() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView1);
        recyclerView.setLayoutManager(layoutManager);
        RecycleAdapter1_home adpter = new RecycleAdapter1_home(this, mNames1, mImageUrls1);
        recyclerView.setAdapter(adpter);
    }

    private void initImage2() {
        mImageUrl2.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_04_hd_pictures_166229.jpg");
        mNames2.add("Da Nang");
        mImageUrl2.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_01_hd_picture_166232.jpg");
        mNames2.add("Da Nang1");
        mImageUrl2.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_02_hd_picture_166206.jpg");
        mNames2.add("Da Nang2");
        mImageUrl2.add("http://images.all-free-download.com/images/graphiclarge/beautiful_nature_landscape_03_hd_picture_166205.jpg");
        mNames2.add("Da Nang2");
        mImageUrl2.add("http://images.all-free-download.com/images/graphiclarge/beautiful_natural_scenery_02_hd_picture_166231.jpg");
        mNames2.add("Da Nang2");
        initRecyleView2();
    }

    private void initRecyleView2() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        RecyclerView recyclerView = findViewById(R.id.recyclerView2);
        recyclerView.setLayoutManager(layoutManager);
        RecycleAdapter1_home adpter = new RecycleAdapter1_home(this, mNames2, mImageUrl2);
        recyclerView.setAdapter(adpter);
    }
}
