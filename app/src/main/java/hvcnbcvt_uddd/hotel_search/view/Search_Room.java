package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.RoomListAdapter;
import hvcnbcvt_uddd.hotel_search.model.Room;

/**
 * Created by NgocTri on 5/23/2018.
 */

public class Search_Room extends AppCompatActivity {
    GridView gridView;
    ArrayList<Room> list;
    RoomListAdapter adapter = null;
    String name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manger_room_list);

        Intent intent= getIntent();
        int id_hot=intent.getIntExtra("Id_hotel",1);
        Toast.makeText(this, "Id hotel :"+id_hot, Toast.LENGTH_SHORT).show();
        gridView = (GridView) findViewById(R.id.gridView);
        list = new ArrayList<>();
        adapter = new RoomListAdapter(Search_Room.this, R.layout.manager_room_uniq, list);
        gridView.setAdapter(adapter);

        try {
            Cursor cursor = Manager_Room.databaseHelper.getData("SELECT * FROM ROOM1 WHERE id_hotel='"+(id_hot+1)+"'");

            list.clear();
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String bed = cursor.getString(3);
                String number = cursor.getString(4);
                String price = cursor.getString(5);
                byte[] image = cursor.getBlob(2);
                String id_hotel = cursor.getString(6);

                list.add(new Room(id, name, image, bed, number, price, id_hotel));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        adapter.notifyDataSetChanged();
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i1= new Intent(Search_Room.this,CartActivity.class);
                int id_room= list.get(position).getId();
                String name= list.get(position).getName();
                String price= list.get(position).getPrice();
                byte[] img= list.get(position).getImage();
                i1.putExtra("Id",id_room);
                i1.putExtra("Name",name);
                i1.putExtra("Price",price);
                i1.putExtra("Image",img);
                startActivity(i1);
            }
        });
    }
}