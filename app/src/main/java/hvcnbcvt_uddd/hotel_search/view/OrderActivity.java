package hvcnbcvt_uddd.hotel_search.view;

/**
 * Created by hoacn on 5/24/2018.
 */

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;

public class OrderActivity extends AppCompatActivity implements View.OnClickListener{
    private EditText edt_name, edt_total, edt_idroom, edt_idcard, edt_email;
    private Button btn_back, btn_order;
    String name, email, card, room, total;
    public static DatabaseHelper sqLiteHelper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking_order_layout);
        edt_name = (EditText) findViewById(R.id.edt_name_order);
        edt_email = (EditText) findViewById(R.id.edt_email_order);
        edt_idcard = (EditText) findViewById(R.id.edt_card_order);
        edt_idroom = (EditText) findViewById(R.id.edt_idroom_order);
        edt_total = (EditText) findViewById(R.id.edt_total_order);
        btn_back = (Button) findViewById(R.id.btnBack_order);
        btn_back.setOnClickListener(this);
        btn_order = (Button) findViewById(R.id.btn_Order);
        btn_order.setOnClickListener(this);
        Intent i = this.getIntent();
        edt_total.setText((String.valueOf( i.getLongExtra("Tong_tien", 0))));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnBack_order:
                this.onBackPressed();
                break;
            case R.id.btn_Order:
                email= edt_email.getText().toString();
                if(isValidEmail(email)){
                    try{
                        sqLiteHelper.insertOrder(
                                edt_name.getText().toString().trim(),
                                edt_idcard.getText().toString().trim(),
                                edt_idroom.getText().toString().trim(),
                                edt_email.getText().toString().trim(),
                                edt_total.getText().toString().trim()
                        );

                        Toast.makeText(getApplicationContext(), "Added successfully!", Toast.LENGTH_SHORT).show();
                        edt_name.setText("");
                        edt_idcard.setText("");
                        edt_idroom.setText("");
                        edt_email.setText("");
                        edt_total.setText("");
                    }
                    catch (Exception e){
                        Toast.makeText(OrderActivity.this, "Add fail! ", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                    Intent order = new Intent(OrderActivity.this, EndActivity.class);
                    startActivity(order);
                    new SendMail().execute("");
                }else {
                    Toast.makeText(OrderActivity.this, "Email không hợp lệ!!! ", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }
    public static boolean isValidEmail(String email) {
        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }
    private class SendMail extends AsyncTask<String, Integer, Void> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = ProgressDialog.show(OrderActivity.this, "Please wait", "Sending mail", true, false);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            progressDialog.dismiss();
        }

        protected Void doInBackground(String... params) {
            Mail m = new Mail("loanntptit@gmail.com", "ooqbeaewfvidhjlk");

            String[] toArr = {email, "youremail@gmail"};
            m.setTo(toArr);
            m.setFrom("loanntptit@gmail.com");
            m.setSubject("This is an email sent using my Mail JavaMail wrapper from an Android device.");
            m.setBody("Email body.");

            try {
                if(m.send()) {
                    Toast.makeText(OrderActivity.this, "Email was sent successfully.", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(OrderActivity.this, "Email was not sent.", Toast.LENGTH_LONG).show();
                }
            } catch(Exception e) {
                Log.e("MailApp", "Could not send email", e);
            }
            return null;
        }
    }
}