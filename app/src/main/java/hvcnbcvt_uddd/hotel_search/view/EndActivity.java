package hvcnbcvt_uddd.hotel_search.view;

/**
 * Created by hoacn on 5/24/2018.
 */

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import hvcnbcvt_uddd.hotel_search.R;

public class EndActivity extends AppCompatActivity implements OnClickListener{
    private Button btnend;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.end_layout);
        btnend = (Button) findViewById(R.id.btnEnd);
        btnend.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(EndActivity.this, MainActivity_home.class);
        startActivity(intent);
    }
}