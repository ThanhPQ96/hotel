package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;

import hvcnbcvt_uddd.hotel_search.R;

/**
 * Created by NgocTri on 5/19/2018.
 */

public class Buoc2_bill extends AppCompatActivity implements View.OnClickListener {
    Button btn_Next_step2,btn_Add_special_request;
    ImageView undo;
    ImageView img_check_Fisrt_Name,img_check_Last_Name,img_check_Email,img_check_SDT,img_check_country;
    EditText edt_FirstName,edt_Last_Name,edt_Email,edt_Country,edt_SDT,edt_Guest_first_Name,edt_Guest_Email;
    CheckBox checkBox_save_detail_for_feature;
    RadioButton radioButton_book_for_me,radioButton_book_for_someone,radioButton_Bussiness,radioButton_Leisure;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_buoc2);
        InitView();
        undo.setOnClickListener(this);
        btn_Next_step2.setOnClickListener(this);
        checkBox_save_detail_for_feature.setOnClickListener(this);

    }

    public void InitView(){
        undo=(ImageView)findViewById(R.id.img_undo_2);
        btn_Add_special_request=(Button)findViewById(R.id.btn_Add_special_request);
        btn_Next_step2=(Button)findViewById(R.id.btn_Next_step_of_2);
        edt_FirstName=(EditText)findViewById(R.id.edt_FirstName);
        edt_Last_Name=(EditText)findViewById(R.id.edt_LastName);
        edt_Email=(EditText)findViewById(R.id.edt_Email);
        edt_SDT=(EditText)findViewById(R.id.edt_Mobile_Phone);
        edt_Country=(EditText)findViewById(R.id.edt_Country_region);
        img_check_Fisrt_Name=(ImageView)findViewById(R.id.imgCheck_FirstName);
        img_check_Last_Name=(ImageView)findViewById(R.id.imgCheck_LastName) ;
        img_check_Email=(ImageView)findViewById(R.id.imgCheck_Email);
        img_check_SDT=(ImageView)findViewById(R.id.imgCheck_MobilePhone);
        img_check_country=(ImageView)findViewById(R.id.imgCheck_Country);
//        edt_Guest_first_Name=(EditText)findViewById(R.id.edt_Booking_for_someone_Guest_Full_Name);
//        edt_Guest_Email=(EditText)findViewById(R.id.edt_Booking_for_someone_guest_email);

        checkBox_save_detail_for_feature=(CheckBox)findViewById(R.id.CheckBox_Save_for_feature);
//        radioButton_book_for_me=(RadioButton)findViewById(R.id.radio_Booking_for_me);
//        radioButton_book_for_someone=(RadioButton)findViewById(R.id.radio_Booking_for_someone_else);
        radioButton_Bussiness=(RadioButton)findViewById(R.id.radio_Primary_purpose_Business);
        radioButton_Leisure=(RadioButton)findViewById(R.id.radio_Primary_purpose_Leisure);

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Next_step_of_2:{
                if(edt_FirstName.getText().equals("")){
                    img_check_Fisrt_Name.setImageResource(R.drawable.warning);
                }
                if (edt_Last_Name.getText().equals("")){
                    img_check_Last_Name.setImageResource(R.drawable.warning);

                }
                if(edt_SDT.getText().equals("")){
                    img_check_SDT.setImageResource(R.drawable.warning);
                }
                if(edt_Country.getText().equals("")){
                    img_check_country.setImageResource(R.drawable.warning);

                }
                if(edt_Email.getText().equals("")){
                    img_check_Email.setImageResource(R.drawable.warning);
                }

                Intent i1= new Intent(this,Buoc3_bill.class);
                startActivity(i1);
                break;
            }
            case R.id.btn_Add_special_request:{
                break;
            }

            case R.id.img_undo_2:{
                Intent i3= new Intent(this,Buoc1_bill.class);
                startActivity(i3);
                break;
            }
//            case R.id.radio_Booking_for_me:{
//                break;
//            }
//            case R.id.radio_Booking_for_someone_else:{
//                break;
//            }
            case R.id.radio_Primary_purpose_Business:{
                break;
            }
            case R.id.radio_Primary_purpose_Leisure:{
                break;
            }
            case R.id.CheckBox_Save_for_feature:{
                break;
            }
            default:
                break;
        }
    }
}
