package hvcnbcvt_uddd.hotel_search.view;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Account;

public class UpadateAccount extends AppCompatActivity implements View.OnClickListener {
    private EditText editText1, editText2, editText3, editText4, editText5, editText6;
    private Button btn1;
    DatabaseHelper helper = new DatabaseHelper(this);
    Account account;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.account_update);

        editText1 = (EditText) findViewById(R.id.ed_email1);
        editText2 = (EditText) findViewById(R.id.ed_pass1);
        editText6 = (EditText) findViewById(R.id.ed_repass1);
        editText3 = (EditText) findViewById(R.id.ed_name1);
        editText4 = (EditText) findViewById(R.id.ed_sdt1);
        editText5 = (EditText) findViewById(R.id.ed_dchi1);
        btn1 = (Button) findViewById(R.id.btn_capnhat);


        btn1.setOnClickListener(this);
        loadData();



    }

    @Override
    public void onClick(View v) {
     try {
     helper = new DatabaseHelper(getApplicationContext());

     Account acc = helper.find(account.getId_account());
     Account temp = helper.Checkuser(editText1.getText().toString());
     if (temp== null){
         acc.setUsername(editText1.getText().toString());
         acc.setUser_pass(editText2.getText().toString());
         acc.setRepass(editText6.getText().toString());
         acc.setFullname(editText3.getText().toString());
         acc.setSdt(Integer.parseInt(editText4.getText().toString()));
         acc.setDiachi(editText5.getText().toString());
         if (helper.update(acc)){
             Intent intent1 = new Intent(UpadateAccount.this, Display.class);
             intent1.putExtra("account", String.valueOf(acc));
             startActivity(intent1);
         }
         else {
             AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
             builder.setTitle("Error");
             builder.setMessage("Loi cap nhat");
             builder.setPositiveButton("oki", new DialogInterface.OnClickListener() {
                 @Override
                 public void onClick(DialogInterface dialog, int which) {
                     dialog.cancel();
                 }
             });
             builder.show();
         }
     }
     else {
         AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
         builder.setTitle("Error");
         builder.setMessage("Da ton tai user");
         builder.setPositiveButton("oki", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 dialog.cancel();
             }
         });
         builder.show();
     }
     }catch (Exception e){
         AlertDialog.Builder builder = new AlertDialog.Builder(v.getContext());
         builder.setTitle("Error");
         builder.setMessage("Da ton tai user");
         builder.setPositiveButton("oki", new DialogInterface.OnClickListener() {
             @Override
             public void onClick(DialogInterface dialog, int which) {
                 dialog.cancel();
             }
         });
         builder.show();
     }
    }
    public void loadData(){
        Intent intent = getIntent();
        account = (Account) intent.getSerializableExtra("account");
        editText1.setText(account.getUsername());
        editText2.setText(account.getUser_pass());
        editText5.setText(account.getRepass());
        editText3.setText(account.getFullname());
        editText4.setText(account.getSdt());
        editText5.setText(account.getDiachi());
    }
}
