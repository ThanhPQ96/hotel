package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageButton;

import hvcnbcvt_uddd.hotel_search.R;

/**
 * Created by NgocTri on 5/19/2018.
 */

public class Buoc3_bill extends AppCompatActivity implements View.OnClickListener {
    Button btn_Book_now;
    ImageButton undo;
    CheckBox checkBox_save_credit_card;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.book_buoc3);
        initView();
        undo.setOnClickListener(this);
        btn_Book_now.setOnClickListener(this);
        checkBox_save_credit_card.setOnClickListener(this);
    }

    public void initView() {
        undo=(ImageButton)findViewById(R.id.img_undo3);
        btn_Book_now = (Button) findViewById(R.id.btn_Book_now);
        checkBox_save_credit_card = (CheckBox) findViewById(R.id.CheckBox_Save_credit_card);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_Book_now: {
                Intent i1 = new Intent(Buoc3_bill.this, Buoc4_bill.class);
                startActivity(i1);
                break;
            }
            case R.id.img_undo3:{
                Intent i2= new Intent(Buoc3_bill.this,Buoc2_bill.class);
                startActivity(i2);
                break;
            }
            case R.id.CheckBox_Save_credit_card: {

                break;
            }
        }
    }
}
