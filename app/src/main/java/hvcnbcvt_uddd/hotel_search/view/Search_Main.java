package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.Switch;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.HotelListAdapter;
import hvcnbcvt_uddd.hotel_search.adapter.RoomListAdapter;
import hvcnbcvt_uddd.hotel_search.model.Hotel;
import hvcnbcvt_uddd.hotel_search.model.Room;

/**
 * Created by NgocTri on 5/23/2018.
 */

public class Search_Main extends AppCompatActivity implements View.OnClickListener{
    ImageButton btnSearch;
    EditText edt_Search_Destination;
    DatePicker startDate,endDate;
    Spinner spinner;
    GridView gridView;
    ArrayList<Hotel> listHotel;
    HotelListAdapter hotelListadapter;
    ArrayList<Room> listRoom;
    RoomListAdapter roomListAdapter;
    int id_hotel;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
    String name;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_main);
        initView();

        id_hotel=0;
        gridView = (GridView)findViewById(R.id.gridView_Search);
        listHotel= new ArrayList<>();
        hotelListadapter = new HotelListAdapter(Search_Main.this,R.layout.manager_hotelitem,listHotel);
        gridView.setAdapter(hotelListadapter);

        try {
            Cursor cursor = MainActivity_home.sqLiteHelper.getData("SELECT * FROM Hotel3");
            listHotel.clear();
            while (cursor.moveToNext()) {
                int id = cursor.getInt(0);
                String name = cursor.getString(1);
                String adress = cursor.getString(2);
                byte[] image = cursor.getBlob(3);
                String number = cursor.getString(4);
                String vote = cursor.getString(5);
                String phoneNumber=cursor.getString(6);
                listHotel.add(new Hotel(name, adress, image, number, vote,phoneNumber));
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        hotelListadapter.notifyDataSetChanged();
        btnSearch.setOnClickListener(this);
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent1= new Intent(Search_Main.this,Search_Room.class);
                id_hotel= listHotel.get(position).getId_hotel();
                intent1.putExtra("Id_hotel",id_hotel);
                startActivity(intent1);
            }
        });

    }

    public void initView(){
        btnSearch=(ImageButton)findViewById(R.id.btn_Search);
        edt_Search_Destination= (EditText)findViewById(R.id.edt__search_Destination);
//        startDate=(DatePicker)findViewById(R.id.Search_date_start);
//        endDate=(DatePicker)findViewById(R.id.Search_date_end);
//        spinner=(Spinner)findViewById(R.id.spinner_Search_room);
        gridView =(GridView)findViewById(R.id.gridView_Search);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_Search: {
//                calendar1.set(startDate.getYear(),startDate.getMonth(),startDate.getDayOfMonth());
//                start =simpleDateFormat.format(calendar1.getTime());
//
//                calendar2.set(endDate.getYear(),endDate.getMonth(),endDate.getDayOfMonth());
//                end =simpleDateFormat.format(calendar2.getTime());
                name =edt_Search_Destination.getText().toString();
                Cursor cursor = MainActivity_home.sqLiteHelper.getData("SELECT * FROM Hotel3 WHERE address_hotel LIKE '%"+name+"%'");
                listHotel.clear();
                while (cursor.moveToNext()) {
                    int id = cursor.getInt(0);
                    String name = cursor.getString(1);
                    String adress = cursor.getString(2);
                    byte[] image = cursor.getBlob(3);
                    String number = cursor.getString(4);
                    String vote = cursor.getString(5);
                    String phonnumer=cursor.getString(6);
                    listHotel.add(new Hotel(name, adress, image, number, vote,phonnumer));
                }
//                hotelListadapter= new HotelListAdapter(this, R.layout.manager_hotelitem, listHotel);
//                gridView.setAdapter(hotelListadapter);

                hotelListadapter.notifyDataSetChanged();
                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i1= new Intent(Search_Main.this,Search_Room.class);
                        startActivity(i1);
                    }
                });

//                Intent i1 = new Intent(Search_Main.this, Search_Hotel.class);
//                String name = edt_Search_Destination.getText().toString();
//                i1.putExtra("name", name);
//                i1.putExtra("start",start);
//                i1.putExtra("end",end);
//                startActivity(i1);
                break;
            }
            default:
                break;
        }
    }

}
