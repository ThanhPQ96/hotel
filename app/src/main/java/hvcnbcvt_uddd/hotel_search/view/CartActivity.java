package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.adapter.CartAdapter;
import hvcnbcvt_uddd.hotel_search.model.Cart;

/**
 * Created by hoacn on 5/23/2018.
 */

public class CartActivity extends AppCompatActivity {
    private ListView lv_cart;
    private Button btn_tieptuc, btn_thanhtoan;
    private TextView tv_thongbao, tv_tongtien;
    private List<Cart> list = new ArrayList<>();
    CartAdapter adapter;
    long sum =0;

    public void onCreate( Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_cart);
        lv_cart = (ListView) findViewById(R.id.lv_cart);
        btn_tieptuc = (Button) findViewById(R.id.btn_tieptuc);
        btn_thanhtoan = (Button) findViewById(R.id.btn_thanhtoan);
        tv_thongbao = (TextView) findViewById(R.id.tv_thongbao);
        tv_tongtien = (TextView) findViewById(R.id.tv_tongtien);
        list = getData();
        check();
        adapter = new CartAdapter(this, list);
        lv_cart.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        for (int i = 0; i<list.size(); i++) {
            sum += list.get(i).getGiasp() *list.get(i).getSoPhong();
        }
        tv_tongtien.setText("" + sum );

        btn_thanhtoan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //TODO thanh toan các đơn đặt phòng
                Intent payIntent = new Intent(CartActivity.this, OrderActivity.class);
                payIntent.putExtra("Tong_tien", sum);
                startActivity(payIntent);
            }
        });
        btn_tieptuc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO quay lại trang tìm kiếm đặt phòng
                Intent back = new Intent(CartActivity.this, MainActivity_search.class);
                startActivity(back);
            }
        });
    }

    private void check() {
        if (MainActivity_home.manggiohang==null){
            tv_thongbao.setVisibility(View.VISIBLE);
            lv_cart.setVisibility(View.INVISIBLE);
        }else {
            tv_thongbao.setVisibility(View.INVISIBLE);
            lv_cart.setVisibility(View.VISIBLE);
        }
    }

    private List<Cart> getData() {
        List<Cart> cartList = new ArrayList<>();
        Intent cartIntent = this.getIntent();
        int id = cartIntent.getIntExtra("Id", 0);
        String name = cartIntent.getStringExtra("Name");
        long price = Long.parseLong(cartIntent.getStringExtra("Price"));
        byte [] image = cartIntent.getByteArrayExtra("Image");
        Cart c = new Cart(id, name, price, image, 1);
        cartList.add(c);
        return cartList;
    }
}
