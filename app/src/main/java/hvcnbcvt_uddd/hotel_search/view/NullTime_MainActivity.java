 package hvcnbcvt_uddd.hotel_search.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import hvcnbcvt_uddd.hotel_search.Connect.DatabaseHelper;
import hvcnbcvt_uddd.hotel_search.R;

 public class NullTime_MainActivity extends AppCompatActivity {

    TextView textView_Room, textView_startDate, textView_endDate;
    EditText edtId_Room, edt_startDate, edt_endDate;
    Button  btnAddNull, btnListNull;


    final int REQUEST_CODE_GALLERY = 999;

    public static DatabaseHelper sqLiteHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.null_time_activity_main);

        init();

        sqLiteHelper = new DatabaseHelper(this, "NullTime.db", null, 1);

        sqLiteHelper.queryData("CREATE TABLE IF NOT EXISTS NullTime(id_nulltime INTEGER PRIMARY KEY AUTOINCREMENT, id_room INTERGER, startDate VARCHAR, endDate VARCHAR)");



        btnAddNull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try{
                    sqLiteHelper.insertDataNullTime(

                            Integer.parseInt(edtId_Room.getText().toString().trim()),
                            edt_startDate.getText().toString().trim(),
                            edt_endDate.getText().toString().trim()

                    );
                    Toast.makeText(getApplicationContext(), "Added successfully!", Toast.LENGTH_SHORT).show();
                    edtId_Room.setText("");
                    edt_startDate.setText("");
                    edt_endDate.setText("");

                }
                catch (Exception e){
                    e.printStackTrace();
                }
            }
        });
        btnListNull.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(NullTime_MainActivity.this, NullTimeList.class);
                startActivity(intent);
            }
        });

    }





    private void init(){

        edtId_Room=(EditText)findViewById(R.id.et_maphong);
        edt_startDate=(EditText)findViewById(R.id.et_ngaybatdau);
        edt_endDate=(EditText)findViewById(R.id.et_ngaytraphong);
        textView_Room=(TextView)findViewById(R.id.tv_maphong);
        textView_startDate=(TextView)findViewById(R.id.tv_ngaybatdau);
        textView_endDate=(TextView)findViewById(R.id.tv_ngaytraphong);
        btnAddNull=(Button)findViewById(R.id.btn_addNull);
        btnListNull=(Button)findViewById(R.id.btn_Listnull);


    }


}
