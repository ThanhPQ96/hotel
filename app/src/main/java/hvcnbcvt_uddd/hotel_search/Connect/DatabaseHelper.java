package hvcnbcvt_uddd.hotel_search.Connect;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import hvcnbcvt_uddd.hotel_search.model.Account;

public class DatabaseHelper extends SQLiteOpenHelper {
    private static final String DB_name = "Hotel.db";
    private static final String Table_name = "Account";
    private static final int VERSION = 1;
    private static final String id_account = "id_account";
    private static final String username = "username";
    private static final String user_pass = "user_pass";
    private static final String fullname = "fullname";
    private static final String sdt = "sdt";
    private static final String diachi = "diachi";
    SQLiteDatabase db;
    public DatabaseHelper(Context context) {
        super(context,DB_name, null, VERSION);
    }

    public DatabaseHelper(Context context,String name, SQLiteDatabase.CursorFactory factory,int version){
        super(context,name,factory,version);
    }

    public void queryData(String sql){
        SQLiteDatabase database = getWritableDatabase();
        database.execSQL(sql);
    }

    public void insertRoom(String name,byte[] image,String bed,String number,String price,String id_hotel ){
        SQLiteDatabase database= getWritableDatabase();
        String sql= "INSERT INTO ROOM1 VALUES (NULL,?,?,?,?,?,?)";
        SQLiteStatement statement= database.compileStatement(sql);
        statement.clearBindings();
        statement.bindString(1,name);
        statement.bindBlob(2,image);
        statement.bindString(3,bed);
        statement.bindString(4,number);
        statement.bindString(5,price);
        statement.bindString(6,id_hotel);

        statement.executeInsert();

    }

    public void updateRoom(String name,byte[] image,String bed,String number,String price,String id_hotel, int id){
        SQLiteDatabase database=getWritableDatabase();
        String sql= "UPDATE ROOM1 SET name_room=?, image_room=?, bed_room=?,number_people=?,price_room=?,id_hotel=? WHERE id_room= ?";
        SQLiteStatement statement= database.compileStatement(sql);

        statement.bindString(1,name);
        statement.bindBlob(2,image);
        statement.bindString(3,bed);
        statement.bindString(4,number);
        statement.bindString(5,price);
        statement.bindString(6,id_hotel);
        statement.bindDouble(7,(double)id);
        statement.execute();
        database.close();

    }

    public void deleteRoom(int id){
        SQLiteDatabase database= getWritableDatabase();

        String sql= "DELETE FROM ROOM1 WHERE id_room= ?";
        SQLiteStatement statement= database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1,(double)id);
        statement.execute();
        database.close();
    }


    public Cursor getRoom(){
        String sql="SELECT * FROM ROOM1";
        SQLiteDatabase database= getWritableDatabase();
        return database.rawQuery(sql,null);

    }


    public void insertData(String name,String address, byte[] image, String numberroom, String vote, String phoneNumber){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO Hotel3 VALUES (NULL, ?, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, name);
        statement.bindString(2, address);
        statement.bindBlob(3, image);
        statement.bindString(4, numberroom);
        statement.bindString(5, vote);
        statement.bindString(6,phoneNumber);

        statement.executeInsert();
    }
    public Cursor getData(String sql){
        SQLiteDatabase database = getReadableDatabase();
        return database.rawQuery(sql, null);
    }
    public  void deleteData(int id) {
        SQLiteDatabase database = getWritableDatabase();

        String sql = "DELETE FROM Hotel2 WHERE id_hotel = ?";
        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();
        statement.bindDouble(1, (double)id);

        statement.execute();
        database.close();
    }

    public void insertDataNullTime(int id_room, String startDate, String endDate){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO NullTime VALUES (NULL, ?, ?, ?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindDouble(1, id_room);
        statement.bindString(2, startDate);
        statement.bindString(3, endDate);

        statement.executeInsert();
    }

    public void onCreate(SQLiteDatabase db) {
        String sql = "create table " + Table_name + "( "+ id_account  + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, "+ username +" TEXT NOT NULL, "+user_pass +" TEXT NOT NULL, "+ fullname +" TEXT NOT NULL, " +sdt+ " INTEGER NOT NULL, "+ diachi+ " TEXT NOT NULL);";
        db.execSQL(sql);
        this.db = db;
    }
    public void insertAccount(Account acc){
        db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        String query = "select * from Account";
        Cursor cursor = db.rawQuery(query, null);
        int count = cursor.getCount();
        values.put(id_account, count);
        values.put(username, acc.getUsername());
        values.put(user_pass, acc.getUser_pass());
        values.put(fullname, acc.getFullname());
        values.put(sdt, acc.getSdt());
        values.put(diachi, acc.getDiachi());
        db.insert(Table_name, null, values);
        db.close();
    }
   public boolean update(Account acc){
        boolean result = true;
        try {
            db = getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put(username, acc.getUsername());
            values.put(user_pass, acc.getUser_pass());
            values.put(fullname, acc.getFullname());
            values.put(sdt, acc.getSdt());
            values.put(diachi, acc.getDiachi());
            result = db.update(Table_name, values, id_account+ " = ?",
                    new String[]{String.valueOf(acc.getId_account())})>0;
        }catch (Exception e){
            result = false;
        }
        return result;
   }
    public Account find(int id){
        Account account = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from "+ Table_name +
                    " where id_account = ?", new String[]{String.valueOf(id)});
            if (cursor.moveToFirst()){
                account = new Account();
                account.setId_account(cursor.getInt(0));
                account.setUsername(cursor.getString(1));
                account.setUser_pass(cursor.getString(2));
                account.setFullname(cursor.getString(3));
                account.setSdt(cursor.getInt(4));
                account.setDiachi(cursor.getString(5));
            }

        }catch (Exception e){
            account = null;
        }
        return account;
    }
    public Account Checkuser(String username){
        Account account = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from "+ Table_name +
                    " where username = ?", new String[]{username});
            if (cursor.moveToFirst()){
                account = new Account();
                account.setId_account(cursor.getInt(0));
                account.setUsername(cursor.getString(1));
                account.setUser_pass(cursor.getString(2));
                account.setFullname(cursor.getString(3));
                account.setSdt(cursor.getInt(4));
                account.setDiachi(cursor.getString(5));
            }

        }catch (Exception e){
            account = null;
        }
        return account;
    }
    public Account Login(String username, String password){
        Account account = null;
        try {
            SQLiteDatabase sqLiteDatabase = getReadableDatabase();
            Cursor cursor = sqLiteDatabase.rawQuery("select * from "+ Table_name +
                    " where username = ? and user_pass = ? ", new String[]{username, password});
            if (cursor.moveToFirst()){
                account = new Account();
                account.setId_account(cursor.getInt(0));
                account.setUsername(cursor.getString(1));
                account.setUser_pass(cursor.getString(2));
                account.setFullname(cursor.getString(3));
                account.setSdt(cursor.getInt(4));
                account.setDiachi(cursor.getString(5));
            }

        }catch (Exception e){
            account = null;
        }
        return account;
    }

    public void insertOrder(String namecard,String numbercard,  String tenphong, String email, String total){
        SQLiteDatabase database = getWritableDatabase();
        String sql = "INSERT INTO Oder VALUES (NULL, ?, ?, ?, ?, ?)";

        SQLiteStatement statement = database.compileStatement(sql);
        statement.clearBindings();

        statement.bindString(1, namecard);
        statement.bindString(2, numbercard);
        statement.bindString(3, tenphong);
        statement.bindString(4, email);
        statement.bindString(5, total);

        statement.executeInsert();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS "+ Table_name;
        db.execSQL(sql);
        onCreate(db);

    }
}
