package hvcnbcvt_uddd.hotel_search.model;

public class Review_search {
    private String reviewName;
    private String reviewComent;
    private  int reviewImage;

    public Review_search(String reviewName, String reviewComent, int reviewImage) {
        this.reviewName = reviewName;
        this.reviewComent = reviewComent;
        this.reviewImage = reviewImage;
    }

    public String getReviewName() {
        return reviewName;
    }

    public void setReviewName(String reviewName) {
        this.reviewName = reviewName;
    }

    public String getReviewComent() {
        return reviewComent;
    }

    public void setReviewComent(String reviewComent) {
        this.reviewComent = reviewComent;
    }

    public int getReviewImage() {
        return reviewImage;
    }

    public void setReviewImage(int reviewImage) {
        this.reviewImage = reviewImage;
    }
}
