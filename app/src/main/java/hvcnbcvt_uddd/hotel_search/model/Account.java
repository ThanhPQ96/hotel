package hvcnbcvt_uddd.hotel_search.model;

public class Account {
    private int id_account, sdt;
    private String username, user_pass, fullname, diachi, repass;
    public Account(){

    }
    public Account(int id_account, int sdt, String username, String user_pass, String fullname, String diachi) {
        this.id_account = id_account;
        this.sdt = sdt;
        this.username = username;
        this.user_pass = user_pass;
        this.fullname = fullname;
        this.diachi = diachi;
    }

    public int getId_account() {
        return id_account;
    }

    public void setId_account(int id_account) {
        this.id_account = id_account;
    }

    public int getSdt() {
        return sdt;
    }

    public void setSdt(int sdt) {
        this.sdt = sdt;
    }

    public Account(int id_account, int sdt, String username, String user_pass, String fullname, String diachi, String repass) {
        this.id_account = id_account;
        this.sdt = sdt;
        this.username = username;
        this.user_pass = user_pass;
        this.fullname = fullname;
        this.diachi = diachi;
        this.repass = repass;
    }

    public String getUsername() {
        return username;

    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRepass() {
        return repass;
    }

    public void setRepass(String repass) {
        this.repass = repass;
    }

    public String getUser_pass() {
        return user_pass;
    }

    public void setUser_pass(String user_pass) {
        this.user_pass = user_pass;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getDiachi() {
        return diachi;
    }

    public void setDiachi(String diachi) {
        this.diachi = diachi;
    }
}
