package hvcnbcvt_uddd.hotel_search.model;

public class Room_search {
    private String id_room;
    private String name_room;
    private String bed_room;
    private String people_room;
    private String id_hotel;
    private float price_room;

    public Room_search(String id_room, String name_room, String bed_room, String people_room, String id_hotel, float price_room) {
        this.id_room = id_room;
        this.name_room = name_room;
        this.bed_room = bed_room;
        this.people_room = people_room;
        this.id_hotel = id_hotel;
        this.price_room = price_room;
    }

    public String getId_room() {
        return id_room;
    }

    public void setId_room(String id_room) {
        this.id_room = id_room;
    }

    public String getName_room() {
        return name_room;
    }

    public void setName_room(String name_room) {
        this.name_room = name_room;
    }

    public String getBed_room() {
        return bed_room;
    }

    public void setBed_room(String bed_room) {
        this.bed_room = bed_room;
    }

    public String getPeople_room() {
        return people_room;
    }

    public void setPeople_room(String people_room) {
        this.people_room = people_room;
    }

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

    public float getPrice_room() {
        return price_room;
    }

    public void setPrice_room(float price_room) {
        this.price_room = price_room;
    }
}
