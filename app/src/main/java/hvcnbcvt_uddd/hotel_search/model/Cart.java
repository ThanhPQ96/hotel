package hvcnbcvt_uddd.hotel_search.model;

public class Cart {
    public int idsp;
    public String tensp;
    public long giasp;
    public byte[] hinhanhsp;
    public int soPhong;

    public Cart() {
    }

    public Cart(int idsp, String tensp, long giasp, byte[] hinhanhsp, int soPhong) {
        this.idsp = idsp;
        this.tensp = tensp;
        this.giasp = giasp;
        this.hinhanhsp = hinhanhsp;
        this.soPhong = soPhong;
    }

    public int getIdsp() {
        return idsp;
    }

    public void setIdsp(int idsp) {
        this.idsp = idsp;
    }

    public String getTensp() {
        return tensp;
    }

    public void setTensp(String tensp) {
        this.tensp = tensp;
    }

    public long getGiasp() {
        return giasp;
    }

    public void setGiasp(long giasp) {
        this.giasp = giasp;
    }

    public byte[] getHinhanhsp() {
        return hinhanhsp;
    }

    public void setHinhanhsp(byte[] hinhanhsp) {
        this.hinhanhsp = hinhanhsp;
    }

    public int getSoPhong() {
        return soPhong;
    }

    public void setSoPhong(int soPhong) {
        this.soPhong = soPhong;
    }
}