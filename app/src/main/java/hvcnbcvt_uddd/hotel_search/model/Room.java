package hvcnbcvt_uddd.hotel_search.model;

/**
 * Created by NgocTri on 5/22/2018.
 */

public class Room {
    private int id;
    private String name,bed;
    private String number,id_hotel;
    private String price;
    private byte[] image;

    public Room(int id, String name, byte[] image, String bed, String number, String price, String id_hotel) {
        this.id = id;
        this.name = name;
        this.bed = bed;
        this.number = number;
        this.id_hotel = id_hotel;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBed() {
        return bed;
    }

    public void setBed(String bed) {
        this.bed = bed;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }
}
