package hvcnbcvt_uddd.hotel_search.model;

public class Hotel_search {
    private String id_hotel;
    private String name_hotel;
    private String address_hotel;
    private byte[] image_hotel;
    private int number_room;
    private String vote_hotel;
    private String phone_number_hotel;


    public Hotel_search(String id_hotel, String name_hotel, String address_hotel, byte[] image_hotel, int number_room, String vote_hotel, String phone_number_hotel) {
        this.id_hotel = id_hotel;
        this.name_hotel = name_hotel;
        this.address_hotel = address_hotel;
        this.image_hotel = image_hotel;
        this.number_room = number_room;
        this.vote_hotel = vote_hotel;
        this.phone_number_hotel = phone_number_hotel;
    }

    public String getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(String id_hotel) {
        this.id_hotel = id_hotel;
    }

    public String getName_hotel() {
        return name_hotel;
    }

    public void setName_hotel(String name_hotel) {
        this.name_hotel = name_hotel;
    }

    public String getAddress_hotel() {
        return address_hotel;
    }

    public void setAddress_hotel(String address_hotel) {
        this.address_hotel = address_hotel;
    }

    public byte[] getImage_hotel() {
        return image_hotel;
    }

    public void setImage_hotel(byte[] image_hotel) {
        this.image_hotel = image_hotel;
    }

    public int getNumber_room() {
        return number_room;
    }

    public void setNumber_room(int number_room) {
        this.number_room = number_room;
    }

    public String getVote_hotel() {
        return vote_hotel;
    }

    public void setVote_hotel(String vote_hotel) {
        this.vote_hotel = vote_hotel;
    }

    public String getPhone_number_hotel() {
        return phone_number_hotel;
    }

    public void setPhone_number_hotel(String phone_number_hotel) {
        this.phone_number_hotel = phone_number_hotel;
    }
}
