package hvcnbcvt_uddd.hotel_search.model;

public class NullTime {
    private int id_nulltime;
    private int id_room;
    private String starttime;
    private String endtime;

    public NullTime() {
    }

    public NullTime(int id_nulltime, int id_room, String starttime, String endtime) {
        this.id_nulltime = id_nulltime;
        this.id_room = id_room;
        this.starttime = starttime;
        this.endtime = endtime;
    }

    public int getId_nulltime() {
        return id_nulltime;
    }

    public void setId_nulltime(int id_nulltime) {
        this.id_nulltime = id_nulltime;
    }

    public int getId_room() {
        return id_room;
    }

    public void setId_room(int id_room) {
        this.id_room = id_room;
    }

    public String getStarttime() {
        return starttime;
    }

    public void setStarttime(String starttime) {
        this.starttime = starttime;
    }

    public String getEndtime() {
        return endtime;
    }

    public void setEndtime(String endtime) {
        this.endtime = endtime;
    }
}