package hvcnbcvt_uddd.hotel_search.model;

public class Hotel {
    private int id_hotel;
    private String name_hotel;
    private String address_hotel;
    private byte[] image_hotel;
    private String number_room;
    private String phoneNumber;
    private String vote;

    public Hotel() {
    }

    public Hotel(int id_hotel, String name_hotel, String address_hotel, byte[] image_hotel, String number_room, String vote,String phoneNumber) {
        this.id_hotel = id_hotel;
        this.name_hotel = name_hotel;
        this.address_hotel = address_hotel;
        this.image_hotel = image_hotel;
        this.number_room = number_room;
        this.vote = vote;
        this.phoneNumber=phoneNumber;
    }

    public Hotel(String name_hotel, String address_hotel, byte[] image_hotel, String number_room, String vote,String phoneNumber) {
        this.name_hotel = name_hotel;
        this.address_hotel = address_hotel;
        this.image_hotel = image_hotel;
        this.number_room = number_room;
        this.vote = vote;
        this.phoneNumber=phoneNumber;
    }

    public int getId_hotel() {
        return id_hotel;
    }

    public void setId_hotel(int id_hotel) {
        this.id_hotel = id_hotel;
    }

    public String getName_hotel() {
        return name_hotel;
    }

    public void setName_hotel(String name_hotel) {
        this.name_hotel = name_hotel;
    }

    public String getAddress_hotel() {
        return address_hotel;
    }

    public void setAddress_hotel(String address_hotel) {
        this.address_hotel = address_hotel;
    }

    public byte[] getImage_hotel() {
        return image_hotel;
    }

    public void setImage_hotel(byte[] image_hotel) {
        this.image_hotel = image_hotel;
    }

    public String getNumber_room() {
        return number_room;
    }

    public void setNumber_room(String number_room) {
        this.number_room = number_room;
    }

    public String getVote() {
        return vote;
    }

    public void setVote(String vote) {
        this.vote = vote;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}