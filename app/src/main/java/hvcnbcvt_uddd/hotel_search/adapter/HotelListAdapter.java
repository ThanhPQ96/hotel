package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Hotel;
import hvcnbcvt_uddd.hotel_search.view.Search_Main;
import hvcnbcvt_uddd.hotel_search.view.Search_Room;

public class HotelListAdapter extends BaseAdapter {
    private Context context;
    private  int layout;
    private ArrayList<Hotel> hotelList;
    @Override
    public int getCount() {
        return hotelList.size();
    }

    public HotelListAdapter(Context context, int layout, ArrayList<Hotel> hotelList) {
        this.context = context;
        this.layout = layout;
        this.hotelList = hotelList;
    }

    @Override
    public Object getItem(int position) {
        return hotelList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    private class ViewHolder{
        ImageView imageView;
        TextView txtName, txtAdress, txtNumber, txtVote,txtPhoneNumber;

    }
    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.txtName = (TextView) row.findViewById(R.id.txtName);
            holder.txtAdress = (TextView) row.findViewById(R.id.txtAddress);
            holder.txtNumber = (TextView) row.findViewById(R.id.txtNumber);
            holder.txtVote = (TextView) row.findViewById(R.id.txtVote);
            holder.imageView = (ImageView) row.findViewById(R.id.imgFood);
            holder.txtPhoneNumber=(TextView)row.findViewById(R.id.txtSDT);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        Hotel food = hotelList.get(position);

        holder.txtName.setText(food.getName_hotel());
        holder.txtAdress.setText(food.getAddress_hotel());
        holder.txtNumber.setText(food.getNumber_room());
        holder.txtVote.setText(food.getVote());
        holder.txtPhoneNumber.setText(food.getPhoneNumber());

        byte[] foodImage = food.getImage_hotel();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        holder.imageView.setImageBitmap(bitmap);

        return row;
    }
}

