package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.ByteArrayInputStream;
import java.text.DecimalFormat;
import java.util.List;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Cart;

/**
 * Created by hoacn on 5/23/2018.
 */

public class CartAdapter extends BaseAdapter {
    private Context mContext;
    private LayoutInflater inflater;
    private List<Cart> cartList;
    int count=0;
    public CartAdapter(Context context, List<Cart> list){
        this.mContext = context;
        this.cartList = list;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return cartList.size();
    }

    @Override
    public Object getItem(int i) {
        return cartList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null){

            inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.item_cart, null);
            ImageView imageView = (ImageView) view.findViewById(R.id.img_phong);
            TextView tenPhong = (TextView) view.findViewById(R.id.tv_tenphong);
            TextView gia = (TextView) view.findViewById(R.id.tv_gia);
            //TextView soluong = (TextView) view.findViewById(R.id.tv_sophong);


            Cart cart = cartList.get(i);
            tenPhong.setText(cart.getTensp());
            DecimalFormat decimalFormat = new DecimalFormat("###,###,###");
            gia.setText(decimalFormat.format(cart.getGiasp()));

            //convert byte to bitmap take from contact class
            byte[] outImage=cart.getHinhanhsp();
            ByteArrayInputStream imageStream = new ByteArrayInputStream(outImage);
            Bitmap theImage = BitmapFactory.decodeStream(imageStream);
            imageView.setImageBitmap(theImage);


            //soluong.setText(""+count);
        }

         return view;
    }
}
