package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Room;

/**
 * Created by NgocTri on 5/22/2018.
 */

public class RoomListAdapter extends BaseAdapter {
   private Context context;
   private int layout;
   private ArrayList<Room> roomList;

    public RoomListAdapter(Context context, int layout, ArrayList<Room> roomList) {
        this.context = context;
        this.layout = layout;
        this.roomList = roomList;
    }

    @Override
    public int getCount() {
        return roomList.size();
    }

    @Override
    public Object getItem(int position) {
        return roomList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder {
        ImageView img;
        TextView txtName,txtBed,txtSoNguoi,txtGia,txtIDHotel;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row= convertView;
        ViewHolder holder= new ViewHolder();
        if(row== null){
            LayoutInflater inflater= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row= inflater.inflate(layout,null);

            holder.txtName=(TextView)row.findViewById(R.id.txt_Room_Name);
            holder.img=(ImageView)row.findViewById(R.id.img_Room_view);
            holder.txtBed=(TextView)row.findViewById(R.id.txt_Room_bed);
            holder.txtGia=(TextView)row.findViewById(R.id.txt_Room_price);
            holder.txtSoNguoi=(TextView)row.findViewById(R.id.txt_Room_number_people);
            holder.txtIDHotel=(TextView)row.findViewById(R.id.txt_Room_id_hotel);
            row.setTag(holder);

        }
        else{
            holder=(ViewHolder)row.getTag();
        }

        Room room= roomList.get(position);

        holder.txtName.setText(room.getName());
        holder.txtBed.setText(room.getBed());
        holder.txtSoNguoi.setText(room.getNumber());
        holder.txtGia.setText(room.getPrice());
        holder.txtIDHotel.setText(room.getId_hotel());

        byte[] roomImage= room.getImage();
        Bitmap bitmap = BitmapFactory.decodeByteArray(roomImage, 0, roomImage.length);

        holder.img.setImageBitmap(bitmap);

        return row;
    }
}
