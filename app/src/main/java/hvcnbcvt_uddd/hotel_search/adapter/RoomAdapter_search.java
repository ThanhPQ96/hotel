package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.listener.OnItemClick;
import hvcnbcvt_uddd.hotel_search.model.Room_search;

public class RoomAdapter_search extends RecyclerView.Adapter<RoomAdapter_search.MyViewHolder> {
    private ArrayList<Room_search> arrRoom ;
    private Context context;
    private OnItemClick onItemClick;
    public RoomAdapter_search(ArrayList<Room_search> arrRoom, Context context, OnItemClick onItemClick){
        this.arrRoom = arrRoom;
        this.context = context;
        this.onItemClick = onItemClick;
    }

    @Override
    public RoomAdapter_search.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_price,parent,false);
        return new RoomAdapter_search.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RoomAdapter_search.MyViewHolder holder, int position) {
        Room_search room = arrRoom.get(position);

        holder.tvNameRoom.setText(room.getName_room());
        holder.tvDesRoom.setText(room.getBed_room());
        holder.tvPriceRoom.setText("$"+room.getPrice_room());
    }

    @Override
    public int getItemCount() {
        return arrRoom.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvNameRoom,tvDesRoom,tvPriceRoom;
        public MyViewHolder(final View itemView) {
            super(itemView);

            tvNameRoom = (TextView) itemView.findViewById(R.id.txtRoom);
            tvDesRoom = (TextView) itemView.findViewById(R.id.txtDescribeRoom);
            tvPriceRoom = (TextView) itemView.findViewById(R.id.txtPriceRoom);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onItemClick.onItemClick(itemView,getLayoutPosition());
                }
            });
        }
    }
}