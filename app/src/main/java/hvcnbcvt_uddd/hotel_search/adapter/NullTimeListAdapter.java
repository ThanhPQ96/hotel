package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.NullTime;



public class NullTimeListAdapter extends BaseAdapter {

    private Context context;
    private  int layout;
    private ArrayList<NullTime> nullTimes;

    public NullTimeListAdapter(Context context, int layout, ArrayList<NullTime> nullTimes) {
        this.context = context;
        this.layout = layout;
        this.nullTimes = nullTimes;
    }

    @Override
    public int getCount() {
        return nullTimes.size();
    }

    @Override
    public Object getItem(int position) {
        return nullTimes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private class ViewHolder{

        TextView txt_maphong, txt_startDate, txt_endDate;
    }

    @Override
    public View getView(int position, View view, ViewGroup viewGroup) {

        View row = view;
        ViewHolder holder = new ViewHolder();

        if(row == null){
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(layout, null);

            holder.txt_maphong = (TextView) row.findViewById(R.id.txt_maphong);
            holder.txt_startDate = (TextView) row.findViewById(R.id.txt_startDate);
            holder.txt_endDate = (TextView) row.findViewById(R.id.txt_endDate);
            row.setTag(holder);
        }
        else {
            holder = (ViewHolder) row.getTag();
        }

        NullTime nullTime = nullTimes.get(position);

        holder.txt_maphong.setText(nullTime.getId_room()+" ");
        holder.txt_startDate.setText(nullTime.getStarttime());
        holder.txt_endDate.setText(nullTime.getEndtime());

        return row;
    }
}
