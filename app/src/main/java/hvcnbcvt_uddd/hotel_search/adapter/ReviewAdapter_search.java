package hvcnbcvt_uddd.hotel_search.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Review_search;

public class ReviewAdapter_search extends RecyclerView.Adapter<ReviewAdapter_search.MyViewHolder> {
    private ArrayList<Review_search> arrReview ;
    private Context context;
    public ReviewAdapter_search(ArrayList<Review_search> arrReview, Context context){
        this.arrReview = arrReview;
        this.context = context;
    }

    @Override
    public ReviewAdapter_search.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_review,parent,false);
        return new ReviewAdapter_search.MyViewHolder(view);
    }


    @Override
    public void onBindViewHolder(ReviewAdapter_search.MyViewHolder holder, int position) {
        Review_search review = arrReview.get(position);
        holder.txtReviewName.setText(review.getReviewName());
        holder.txtReviewComent.setText(review.getReviewComent());
        holder.imgReview.setImageResource(review.getReviewImage());
    }

    @Override
    public int getItemCount() {
        return arrReview.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtReviewName, txtReviewComent;
        ImageView imgReview;
        public MyViewHolder(View itemView) {
            super(itemView);

            txtReviewName = (TextView) itemView.findViewById(R.id.txtReviewName);
            txtReviewComent = (TextView) itemView.findViewById(R.id.txtReviewComent);
            imgReview =  itemView.findViewById(R.id.imgReview);
        }
    }
}

