package hvcnbcvt_uddd.hotel_search.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import hvcnbcvt_uddd.hotel_search.R;
import hvcnbcvt_uddd.hotel_search.model.Hotel_search;
import hvcnbcvt_uddd.hotel_search.view.ActivityDetail_search;

public class HotelAdapter_search extends RecyclerView.Adapter<HotelAdapter_search.MyViewHolder> {
    private ArrayList<Hotel_search> arrHotelSearch;
    private Context context;

    public HotelAdapter_search(ArrayList<Hotel_search> arrHotelSearch, Context context) {
        this.arrHotelSearch = arrHotelSearch;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.search_item, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Hotel_search hotelSearch = arrHotelSearch.get(position);
//        Log.d("bin","ok123");
        holder.tvName.setText(hotelSearch.getName_hotel());
        holder.tvDes.setText(hotelSearch.getAddress_hotel());
        holder.tvPrice.setText(hotelSearch.getPhone_number_hotel());
        holder.tvReview.setText(hotelSearch.getVote_hotel() + " review");
        byte[] foodImage = hotelSearch.getImage_hotel();
        Bitmap bitmap = BitmapFactory.decodeByteArray(foodImage, 0, foodImage.length);
        holder.imHotel.setImageBitmap(bitmap);


        holder.tvPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + hotelSearch.getPhone_number_hotel()));
                if (ActivityCompat.checkSelfPermission(view.getContext(), Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                context.startActivity(intent);
            }
        });
    }
    public void setFilter(ArrayList<Hotel_search> dsks){
        arrHotelSearch=new ArrayList<>();
        arrHotelSearch.addAll(dsks);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return arrHotelSearch.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
        TextView tvName, tvDes, tvPrice, tvReview;
        Button btnDetail;
        ImageView imHotel;
        public MyViewHolder(View itemView) {
            super(itemView);

            tvName = (TextView) itemView.findViewById(R.id.txtNameHotel);
            tvDes = (TextView) itemView.findViewById(R.id.txtDescribe);
            tvPrice = (TextView) itemView.findViewById(R.id.txtPrice);
            tvReview = (TextView) itemView.findViewById(R.id.txtReview);
            btnDetail = itemView.findViewById(R.id.btnDetail);
            imHotel = itemView.findViewById(R.id.im_hotel);


            btnDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent ActivityDetail = new Intent(context, ActivityDetail_search.class);
                    context.startActivity(ActivityDetail);
                }
            });
        }
    }
}
